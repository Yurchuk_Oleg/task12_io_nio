package com.epam.yurchuk.serverV1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThread extends Thread {
    PrintWriter out;
    BufferedReader in;

    public ServerThread(Socket socket) {

        try {
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        out.println("Hello");
        while (true) {
            try {
                String s = in.readLine();
                char[] c_arr = new char[s.length()];
                for (int i = 0; i < c_arr.length; i++) {
                    c_arr[i] = s.charAt(i);
                }
                int max_frequency = 0, count = 0;
                char symbol = c_arr[0];
                for (int i = 0; i < c_arr.length; i++) {
                    if (c_arr[i] == ' ') {
                        continue;
                    }
                    count = 0;
                    for (int j = 0; j < c_arr.length; j++) {
                        if (c_arr[j] == c_arr[i]) {
                            ++count;
                        }
                    }
                    if (count > max_frequency) {
                        max_frequency = count;
                        symbol = c_arr[i];
                    }
                }
                s = ">>> " + symbol + " occurred " + max_frequency + " time(s)";
                if (s.trim().isEmpty()) break;
                out.println(">>> " + s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
