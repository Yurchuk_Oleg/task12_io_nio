package com.epam.yurchuk.serverV1;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ServerMain {
    public static void main(String[] args) {
        new ServerMain().run();
    }

    private void run() {
        int count = 0;
        try {
            ServerSocket serverSocket = new ServerSocket(1234);
            System.out.println("Server Started");

            while (true) {
                Socket socket = serverSocket.accept();
                ServerThread st = new ServerThread(socket);
                st.start();
                System.out.println("Client " + count++ + " connected");
                System.out.println("New connection from: " + socket.getRemoteSocketAddress().toString());
                Date d1 = new Date();
                SimpleDateFormat df = new SimpleDateFormat("MM/dd/YYYY HH:mm a");
                String formattedDate = df.format(d1);
                System.out.println("Hello, Oleg Yurchuk. Date: " + formattedDate);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("disconnecting...");
    }
}
