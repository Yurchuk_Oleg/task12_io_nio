package com.epam.yurchuk.serialization.content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Ship implements Serializable {
    private String name;
    private String side;
    private List<Droid> droids;

    public Ship(String name, String side) {
        this.name = name;
        this.side = side;
        this.droids = CreateDroids();

    }

    public static List<Droid> CreateDroids() {
        return new ArrayList<Droid>(Arrays.asList(
                new Droid(0, "Droid[1]", 50.5, 1),
                new Droid(0, "Droid[2]", 60.4, 1),
                new Droid(0, "Droid[3]", 70.6, 1),
                new Droid(0, "Droid[4]", 80.7, 1),
                new Droid(0, "Droid[5]", 100.5, 2)
        ));
    }

    public List<Droid> getDroids() {
        return droids;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "name='" + name + '\'' +
                ", side='" + side + '\'' +
                ", droids=" + droids +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ship ship = (Ship) o;
        return Objects.equals(name, ship.name) &&
                Objects.equals(side, ship.side) &&
                Objects.equals(droids, ship.droids);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, side, droids);
    }
}
