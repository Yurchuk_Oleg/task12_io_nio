package com.epam.yurchuk.serialization.content;

import java.io.Serializable;

public class Droid implements Serializable {
    private int id;
    private String name;
    private double damage;
    private transient int transientValue;

    public Droid(int id, String name, double damage, int transientValue) {
        this.id = id;
        this.name = name;
        this.damage = damage;
        this.transientValue = transientValue;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getDamage() {
        return damage;
    }

    public int getTransientValue() {
        return transientValue;
    }

    @Override
    public String toString() {
        return "Droids{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", damage=" + damage +
                ", transientValue=" + transientValue +
                '}';
    }
}
