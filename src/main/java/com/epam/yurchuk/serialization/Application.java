package com.epam.yurchuk.serialization;

import com.epam.yurchuk.serialization.content.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Application {
    private static final String file = "ser.ser";
    private static final Logger log = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        Ship ship = new Ship("Collector 228", "Dark");
        log.info(ship);

        try (ObjectOutputStream outObj = new ObjectOutputStream(new FileOutputStream(file))) {
            outObj.writeObject(ship);
            log.info("Object has been serialized");
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream inputObj = new ObjectInputStream(new FileInputStream(file))) {
            Ship ship1 = (Ship) inputObj.readObject();
            log.info("Object has been deserialized");
            log.info(ship1);
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

    }
}