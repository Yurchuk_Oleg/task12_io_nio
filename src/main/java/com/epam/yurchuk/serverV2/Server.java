package com.epam.yurchuk.serverV2;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Server {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        try (
                Socket clientSocket = serverSocket.accept();
                PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        ) {
            System.out.println("New connection from: " + clientSocket.getRemoteSocketAddress().toString());
            Date d1 = new Date();
            SimpleDateFormat df = new SimpleDateFormat("MM/dd/YYYY HH:mm a");
            String formattedDate = df.format(d1);
            out.println("Hello, Oleg Yurchuk. Date: " + formattedDate);
            String inputLine;

            if ((inputLine = in.readLine()) != null) {
                System.out.println("Have request from client: " + inputLine);
                char[] c_arr = new char[inputLine.length()];
                for (int i = 0; i < c_arr.length; i++) {
                    c_arr[i] = inputLine.charAt(i);
                }
                int max_frequency = 0, count = 0;
                char symbol = c_arr[0];
                for (int i = 0; i < c_arr.length; i++) {
                    if (c_arr[i] == ' ') {
                        continue;
                    }
                    count = 0;
                    for (int j = 0; j < c_arr.length; j++) {
                        if (c_arr[j] == c_arr[i]) {
                            ++count;
                        }
                    }
                    if (count > max_frequency) {
                        max_frequency = count;
                        symbol = c_arr[i];
                    }
                }
                System.out.println('"' + inputLine + '"' + " -> " + '"' + symbol + '"' + " occurred " + max_frequency + " time(s)");
            }
        } catch (IOException e) {
            System.out.println("Connection error: " + e.getMessage());
        }
        System.out.println("Client has been disconnected");
    }
}