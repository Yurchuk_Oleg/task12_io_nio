package com.epam.yurchuk.serverV2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        try (
                Socket socket = new Socket("127.0.0.1", 8080);
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ) {
            String inputLine;
            if ((inputLine = in.readLine()) != null) {
                System.out.println(inputLine);
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter text (request):");
            out.println(scanner.nextLine());
            Thread.sleep(2000);
            if ((inputLine = in.readLine()) != null) {
                System.out.println("have received from server: " + inputLine);
            }
        } catch (Throwable e) {

        }
        System.out.println("disconnecting...");
    }
}