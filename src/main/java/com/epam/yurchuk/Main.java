package com.epam.yurchuk;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        OutputStream out = new FileOutputStream("file.out.txt");
        PrintStream pout = new PrintStream(out);
        System.setOut(pout);
        //коментар для таску
        /*
            коментарі для таску
         */
        System.out.println("Hello my name is Oleh");
        System.out.println("19 y.o");
        //some comment
        System.out.println("How are you?");
    }
}
