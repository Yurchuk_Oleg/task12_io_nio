package com.epam.yurchuk.specdir;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Date;

public class Application {
    private final static Logger log = LogManager.getLogger(Application.class);
    private final static String path = "C:\\Users\\38067\\Desktop\\term 3\\ComputerGraphic\\Part A";

    public static void main(String[] args) {
        File file = new File(path);
        if (file.exists()) {
            displayFile(file, "");
        } else {
            log.info("file not exists");
        }
    }

    private static void displayFile(File file, String s) {
        File[] files = file.listFiles();
        s += " ";
        for (File f : files) {
            if (f.isDirectory()) {
                log.info(s + f.getPath());
                displayFile(f, s);
            } else {
                displayInfo(s, f);
            }
        }
    }

    private static void displayInfo(String s, File f) {
        log.info(s + f.getName()
                + ", EXECUTE: " + f.canExecute()
                + ", WRITE: " + f.canWrite()
                + ", READ: " + f.canRead()
                + ", is hidden - " + f.isHidden()
                + ", last modified - " + new Date(f.lastModified())
        );
    }
}
